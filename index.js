console.log("Pokemon, Gonna Catch 'em ALL!")

let pokemonTrainer = {
	name : "Ash Ketchum",
	age : 10,
	pokemons: ["Pickachu", "Charizard", "Squirtle", "Balbasaur"],
	friends : {
		hoenn : ["May", "Max"],
		kanto : ["Brock", "Misty"],
	},	
	talk : function(){
			console.log("Pickachu! I choose you!")
		}
	}

console.log(pokemonTrainer);


console.log("Result of dot notation: ");
console.log(pokemonTrainer.name);
console.log("Result of square bracket notation: ");
console.log(pokemonTrainer["pokemons"]);
console.log("Result of talk method: ");
pokemonTrainer.talk();



function Pokemon(name, level){
	this.name = name;
	this.level = level;
	this.health = level*2;
	this.attack = level;

	this.tackle = function(target){
		console.log(this.name + ' tackled ' + target.name);
		target.health -= this.attack;
		console.log( target.name + " health is now reduced to "+ target.health)
	}
	this.faint = function(){
		console.log(this.name + " fainted");
	}
}
let pickachu = new Pokemon("Pickachu ", 12);
console.log(pickachu);

let geodude = new Pokemon("Geodude", 8);
console.log(geodude);

let mewtwo = new Pokemon("Mewtwo", 100);
console.log(mewtwo);

geodude.tackle(pickachu);

console.log(pickachu);

mewtwo.tackle(geodude);
geodude.faint();

console.log(geodude);
